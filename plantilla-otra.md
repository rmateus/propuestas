# Título de la propuesta

Pequeña introducción y motivación de la misma.

## Tipo de actividad

Describe de qué tipo de actividad se trata.

## Descripción

Descripción de un par de párrafos sobre de qué va la propuesta.

## Público objetivo

¿A quién va dirigida? 

## Ponente(s)

¿Quién o quienes van a dar la charla? ¿Qué hacen? ¿Qué charlas han
dado antes?

## Requisitos para los asistentes

Describe los requisitos para participar en la actividad. Por ejemplo, traer un teléfono móvil, un paraguas y una linterna.

## Requisitos para la organización

Describe los medios materiales que necesitas de la organización. Por ejemplo, una sala con mesas y sillas, una sala de ordenadores, una pizarra...

## Comentarios

Cualquier otro comentario relevante.

## Condiciones

* [ ] Acepto seguir el [código de conducta](https://eslib.re/2019/conducta/) y solicitar a los asistentes y ponentes esta aceptación.
* [ ] Al menos una persona entre los que la proponen estará presente el día programado para la charla.


